# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Eazy Chat Room
* Key functions (add/delete)
    1. 與他人聊天
    2. 載入歷史記錄
    3. 全域聊天
    
* Other functions (add/delete)
    1. 第三方登入


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|Firebase Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://midterm-8cfeb.firebaseapp.com/index.html

# Components Description : 
1. 全域聊天 : <br>
    在index.html中，有兩欄input，分別為「尋找聊天對象」與「送出訊息」。<br>
    在一登入進該頁面時，畫面中會顯示「大廳」的聊天紀錄。這項功能是在database中開始一個node，稱之為「lobby」。「lobby」中會記錄每條訊息，訊息的內容會有"message"及"from"：<br>
    1.message記錄由「送出訊息」給出的文字內容<br>
    2.from記錄該筆資料為哪個使用者送出<br>
    
2. 載入歷史紀錄 :<br>
    這個聊天室有兩種聊天方式，「一對一」與「大廳聊天」。<br>
    從signin到index時，會先到「大廳聊天」，比照Lab6中「New Post」的方式，將記錄在「lobby」的message讀取出來，並透過「from」去控制div的style。<br>
3. 與他人聊天 :<br>
    如果點選「Chat」的按鈕，會配對到input中所輸入的value，如果使用者不存在會跳出alert，如果使用者存在的話，會進入到與他之間的聊天室。<br>
    一對一和全域聊天一樣，皆會記錄message跟from。不過一對一在記錄時，會同時在database中兩方的node下做記錄，確保能讓兩方讀取到相同的資訊。

# Other Functions Description(1~10%) : 


## Security Report (Optional)
